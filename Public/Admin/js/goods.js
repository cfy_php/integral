  var specNum = 0; 
   function checkShow() {
     var val=$('input:radio[name="limit_time"]:checked').val();
     if(val == 1){
        $('#tickets').show();
        $('#pointName').show();
        SpecMap(map);
        $(".point").css('display','block');        
     }else if(val == 0){
       $('#tickets').show();
       $('#inter').show();
     }else{
       $('#tickets').hide();
       $('#inter').hide();
     }
   }

   function onShow() {
     var val=$('input:radio[name="limit_time"]:checked').val();
     if(val == 0){
        $('input:radio[name="limit_time"]').val(1);
        $('#tickets').show(); 
     }else{
       $('input:radio[name="limit_time"]').val(0);
       $('input:radio[name="limit_time"]').attr("checked",false);
       $('#tickets').hide(); 
     }
   }
/**生成销售规格表**/
function addSpecSaleCol(){
  //获取规格分类和规格值
  var catId,cateId,snum,specCols = {},obj = [];
  $('.j-speccat').each(function(){
    if($(this)[0].checked){
      catId = $(this).attr('catId');
      cateId = $(this).attr('cateId');
      snum = $(this).attr('cate_name');
        if(!specCols[catId]){
          specCols[catId] = [];
          specCols[catId].push({id:cateId,val:$.trim($('#specName_'+cateId).text())});
        }else{
          specCols[catId].push({id:cateId,val:$.trim($('#specName_'+cateId).text())});
        }
      }
  });

  //创建表头
  $('.j-saleTd').remove();
  var html = [],specArray = [];
  for(var key in specCols){
    html.push('<th class="j-saleTd">'+$('#specCat_'+key).html()+'</th>');
    specArray.push(specCols[key]);
  }
 
  $(html.join('')).insertBefore('#thCol');
  //组合规格值
  this.combined = function(doubleArrays){
        var len = doubleArrays.length;
        if (len >= 2) {
            var arr1 = doubleArrays[0];
            var arr2 = doubleArrays[1];
            var len1 = doubleArrays[0].length;
            var len2 = doubleArrays[1].length;
            var newlen = len1 * len2;
            var temp = new Array(newlen),ntemp;
            var index = 0;
            for (var i = 0; i < len1; i++) {
              if(arr1[i].length){
                for (var k = 0; k < len2; k++) {
                  ntemp = arr1[i].slice();
                  ntemp.push(arr2[k]);
                    temp[index] = ntemp;
                    index++;
                }
              }else{
                  for (var j = 0; j < len2; j++) {
                      temp[index] = [arr1[i],arr2[j]];
                      index++;
                  }
              }
            }
            var newArray = new Array(len - 1);
            newArray[0] = temp;
            if (len > 2) {
                var _count = 1;
                for (var i = 2; i < len; i++) {
                    newArray[_count] = doubleArrays[i];
                    _count++;
                }
            }
            return this.combined(newArray);
        }else {
            return doubleArrays[0];
        }
    }

  var specsRows = this.combined(specArray);
  //生成规格值表
  html = [];
  var id=[],key=1,specHtml = [];
  var str = '';
  for(var i=0;i<specsRows.length;i++){
    id = [],specHtml = [];
    html.push('<tr class="j-saleTd">');
    
    if(specsRows[i].length){
      for(var j=0;j<specsRows[i].length;j++){
        id.push(specsRows[i][j].id);
        var specId = specsRows[i][j].id;
        specHtml.push('<td><input type="hidden" id="mapping_id_'+specId+'_'+i+'" name="mapping_id[spec_'+specId+'_'+i+']" value="'+specId+'"/>' + specsRows[i][j].val + '</td>');

       }
    }else{
      id.push(specsRows[i].id);
      var specId = specsRows[i].id;
      specHtml.push('<td><input type="hidden" id="mapping_id_'+specId+'_'+i+'" name="mapping_id[spec_'+specId+'_'+i+']" value="'+specId+'"/>' + specsRows[i].val + '</td>');
    }
    // id = id.join('-');
    html.push('  <td><input type="hidden" id="set_id_'+i+'" name="set_id_'+i+'"/>'+(i+1)+'</td>');
    html.push(specHtml.join(''));
    html.push('  <td><input type="hidden" class="form-control" id="goods_num_'+i+'" name="goods_number_'+i+'"><span id="goods_number_'+i+'"></span></td>',
                ' <td><input type="hidden" class="form-control" id="goods_amount_'+i+'" name="goods_amount_'+i+'">'+
                ' <input type="text" class="form-control" id="limit_num_'+i+'" name="limit_num_'+i+'">(最多可填<span id="goods_count_'+i+'"></span>)</td>',
                ' <td><input type="text" class="form-control" id="goods_point_'+i+'" name="goods_point_'+i+'"></td>',
                ' <td class="point"><input type="text" class="form-control" id="limit_point_'+i+'" name="limit_point_'+i+'"></td>',
                '</tr>');
    key++;
  }
  $('#spec-sale-tby').append(html.join(''));
  //设置销售规格表值
  fillSepcSale(saleSpec);
}




function saveSpec() {
    var url = $('#FormData').attr("action");
    var data = $('#FormData').serialize();
    $.post(url,data,function(res){
      if(res.status == 1){
          layer.msg('添加成功', { icon: 1, time: 1000 });
          window.location.href=res.url;
          return false;
      }else{
        layer.msg(res.msg, { icon: 2, time: 1000 });
        return false;
      }
    }, "json");
}





/**修改时，给商品规格添值**/
function SpecMap(SpecMap) {
   $.each(SpecMap,function(arr, value){ 
      $.each(value.mapping_id,function(j,set) {
        $('.j-spec_'+set.mapping_id).attr('checked',true);
      });
      addSpecSaleCol();
  });
}


function fillSepcSale(saleSpec){
  $.each(saleSpec,function(i, value){ 
       $('#goods_num_'+i).attr("value",value.goods_number);
        $('#goods_number_'+i).html(value.goods_number);
        $('#goods_amount_'+i).attr("value",value.goods_amount);
        $('#goods_count_'+i).html(value.goods_amount);
        $('#set_id_'+i).attr("value",value.set_id);
        $('#limit_num_'+i).attr("value",value.limit_num);
        $('#goods_point_'+i).attr("value",value.goods_point);
        $('#limit_point_'+i).attr("value",value.limit_point);
  });
}


  $(function () {
    $('.form_datetime').datetimepicker({
      language: 'zh-CN',
      pickerPosition:"bottom-left",
      weekStart: 1,
      todayBtn:  1,
      autoclose: 1,
      todayHighlight: 1,
      startView: 2,
      forceParse: 0,
      minView:2,
      showMeridian: false,
      //startDate:'+0d'
    });   
  });  

   function submitGoods(){
    var url = $('#FormData').attr("action");
    var data = $('#FormData').serialize();
    $.post(url,data,function(res){
      if(res.status == 1){
          layer.msg('添加成功', { icon: 1, time: 1000 });
          $('#productNo').val(res.data);
          return false;
      }else if(res.status == 2){
          layer.msg('修改成功', { icon: 1, time: 1000 });
          if(res.url){
            window.location.href = res.url;
          }
          return false;
      }else{
        layer.msg(res.info, { icon: 2, time: 1000 });
        return false;
      }
    }, "json");

   }