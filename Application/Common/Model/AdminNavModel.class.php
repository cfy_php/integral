<?php
namespace Common\Model;
use Common\Model\BaseModel;
/**
 * 菜单操作model
 */
class AdminNavModel extends BaseModel{

	/**
	 * 删除数据
	 * @param	array	$map	where语句数组形式
	 * @return	boolean			操作是否成功
	 */
	public function deleteData($map){
		$count=$this
			->where(array('pid'=>$map['id']))
			->count();
		if($count!=0){
			return false;
		}
		$this->where(array($map))->delete();
		return true;
	}

	/**
	 * 获取全部菜单
	 * @param  string $type tree获取树形结构 level获取层级结构
	 * @return array       	结构数据
	 */
	public function getTreeData($type='tree',$order=''){
		// 判断是否需要排序
		if(empty($order)){
			$data=$this->select();
		}else{
			$data=$this->order('order_number is null,'.$order)->select();
		}
		$userid = session('aid');
		// 获取树形或者结构数据
		if($type=='tree'){
			$data=\Org\Nx\Data::tree($data,'name','id','pid');
		}elseif($type="level"){
			$data=\Org\Nx\Data::channelLevel($data,0,'&nbsp;','id');
			// 显示有权限的菜单
			$auth=new \Think\Auth();
			foreach ($data as $k => $v) {
				if ($auth->check($v['mca'],$userid)) {
					foreach ($v['_data'] as $m => $n) {
						if(!$auth->check($n['mca'],$userid)){
							unset($data[$k]['_data'][$m]);
						}
					}
				}else{
					// 删除无权限的菜单
					unset($data[$k]);
				}
			}
			
		}
		// print_r($data);exit;
		return $data;
	}
   
    public function getNav($data){
        $list = array();
        $system = C('SYSTEM_TEXT');
    	foreach ($data as $key => $value) {
           $list[] = $system[$value['type']];
    	} 

        $nav = $this->more_array_unique($list);
         return $nav;  
    }


	public function more_array_unique($arr=array()){  
		    foreach($arr[0] as $k => $v){  
		        $arr_inner_key[]= $k;   //先把二维数组中的内层数组的键值记录在在一维数组中  
		    }  
		    foreach ($arr as $k => $v){  
		        $v =implode(",",$v);    //降维 用implode()也行  
		        $temp[$k] =$v;      //保留原来的键值 $temp[]即为不保留原来键值  
		    }  
		    $temp =array_unique($temp);    //去重：去掉重复的字符串  
		    foreach ($temp as $k => $v){  
		        $a = explode(",",$v);   //拆分后的重组 如：Array( [0] => james [1] => 30 )  
		        $arr_after[$k]= array_combine($arr_inner_key,$a);  //将原来的键与值重新合并  
		    }  
			$sort = array(  
			        'direction' => 'SORT_ASC', //排序顺序标志 SORT_DESC 降序；SORT_ASC 升序  
			        'field'     => 'order',       //排序字段  
			);  
			$arrSort = array();  
			foreach($arr_after as $uniqid => $row){  
			    foreach($row as $key=>$value){  
			        $arrSort[$key][$uniqid] = $value;  
			    }  
			}  
			if($sort['direction']){  
			    array_multisort($arrSort[$sort['field']], constant($sort['direction']), $arr_after);  
			}  

		    return $arr_after;  
		}  

}
