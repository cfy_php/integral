<?php
namespace Common\Model;
use Think\Model;
use Think\Upload;

/**
 * 文件模型
 * 负责文件的下载和上传
 */

class GoodsBasicInfoModel extends Model{
     // 自动验证
    protected $_validate=array(
        // array('banner_type','require','所属栏目必填'), // 验证字段必填
        // array('url','require','链接地址必填'), // 验证字段必填
        array('goods_desc','require','商品介绍必填'), // 验证字段必填
    );   
    protected $_auto=array(
        array('create_time', 'date',1,'function',array('Y-m-d H:i:s')),
        array('update_time', 'date',1,'function',array('Y-m-d H:i:s')),
        );
    /**
     * 文件上传
     * @param  array  $files   要上传的文件列表（通常是$_FILES数组）
     * @param  array  $setting 文件上传配置
     * @param  string $driver  上传驱动名称
     * @param  array  $config  上传驱动配置
     * @return array           文件上传成功后的信息
     */
    public function uploadPicture($files, $setting, $driver = 'Local', $config = null){
        /* 上传文件 */
        $Upload = new Upload($setting, $driver, $config);
        $info   = $Upload->upload($files);
        /* 设置文件保存位置 */
        $this->_auto[] = array('location', 'Ftp' === $driver ? 1 : 0, self::MODEL_INSERT);
        if($info){ //文件上传成功，记录文件信息
            foreach ($info as $key => &$value) {
                /* 记录文件信息 */
                $value['filespath'] = substr($setting['rootPath'], 1).$value['savepath'].$value['savename']; //在模板里的url路径
                if(D('FileInfo')->create($value) && ($id = D('FileInfo')->add())){
                    $value['id'] = $id;
                } 
            }
            return $info; //文件上传成功
        } else {
            $this->error = $Upload->getError();
            return false;
        }
    }

     /**
     * 添加用户
     */
    public function addData($data){
        // 对data数据进行验证
        // $data['goods_desc']=htmlspecialchars_decode($data['goods_desc']);
        // // 转义
        // $data['goods_desc']=htmlspecialchars($data['goods_desc']);
        // print_r($data);exit;
        if(!$data=$this->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            // 验证通过
            $result=$this->add($data);
            return $result;
        }
    }

    /**
     * 修改用户
     */
    public function editData($map,$data){
        // 对data数据进行验证
        if(!$data=$this->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            // 验证通过
            $result=$this
                ->where($map)
                ->save($data);
            return $result;
        }
    }


    // 获取商品分页数据
    public function getPageData($map){
        $count = M('GoodsBasicInfo')->where($map)->count(); 
        $page=new \Org\Bjy\Page($count,20);
        $list= M('GoodsBasicInfo')
            ->where($map)
            ->order('create_time desc')
            ->limit($page->firstRow.','.$page->listRows)
            ->select();
        $data=array(
            'data'=>$list,
            'page'=>$page->show()
            );
        return $data;

    }


    // 获取商品数据
    public function getPageList($map,$res){
        $map['goods_type'] = 1;
        $res['goods_type'] = 1;
        $count= M('GoodsBasicInfo as b')->where($map)->count();
        $page=new \Org\Bjy\Page($count,20);
        $list= M('GoodsBasicInfo as b')  
             ->where($map)
             ->order('create_time desc')
             ->limit($page->firstRow.','.$page->listRows)
             ->select();    
        if($count > 0){
            $page=new \Org\Bjy\Page($count,20);
            $list= M('GoodsBasicInfo as b')  
                 ->where($map)
                 ->order('create_time desc')
                 ->limit($page->firstRow.','.$page->listRows)
                 ->select();   
            $type = array('1'=>'实物','2'=>'虚拟物');
            foreach ($list as $key => $value) {
               $list[$key]['type'] = $type[$value['goods_type']];
            }            
        }else{     
            $count2 = M('GoodsBasicInfo as b')
                  ->join('t_goods_spec_set_info as s on s.goods_id = b.goods_id')   
                  ->where($res)->count();           
            $page=new \Org\Bjy\Page($count2,20);
            $list= M('GoodsBasicInfo as b')
                 ->join('t_goods_spec_set_info as s on s.goods_id = b.goods_id')        
                 ->where($res)
                 ->field('b.goods_id,b.goods_name,b.goods_type,goods_brand')
                 ->order('b.create_time desc')
                 ->limit($page->firstRow.','.$page->listRows)
                 ->select();   
            $type = array('1'=>'实物','2'=>'虚拟物');
            foreach ($list as $key => $value) {
               $list[$key]['type'] = $type[$value['goods_type']];
            }                
        }
        $data=array(
            'data'=>$list,
            'page'=>$page->show()
            );
        return $data;

    }
  
    /**
     * 删除数据
     * @param   array   $map    where语句数组形式
     * @return  boolean         操作是否成功
     */
    public function deleteData($map){
        $count=$this
            ->where(array('pid'=>$map['id']))
            ->count();
        if($count!=0){
            return false;
        }
        $this->where(array($map))->delete();
        return true;
    }


}
