<?php
namespace Common\Model;
use Common\Model\BaseModel;
/**
 ** 产品Model
 */
class OrderExpressInfoModel extends BaseModel{
    protected $trueTableName='dbcfy.t_order_express_info';
    // 自动验证
    // 自动完成
    protected $_auto=array(
        array('create_time', 'date',1,'function',array('Y-m-d H:i:s')),
        array('update_time', 'date',1,'function',array('Y-m-d H:i:s'))
        );
     /**
     * 添加用户
     */
    public function addData($data){
        // 对data数据进行验证
        if(!$data=$this->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            // 验证通过
            $result=$this->add($data);
            return $result;
        }
    }

    /**
     * 修改产品
     */
    public function editData($map,$data){
        // 对data数据进行验证
        if(!$data=$this->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            // 验证通过
            $result=$this
                ->where($map)
                ->save($data);
            return $result;
        }
    }

    /**
     * 修改产品
     */
    public function editDataInfo($map,$data){
        // 对data数据进行验证
        if(!$data=$this->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            // 验证通过
            $result=$this
                ->where($map)
                ->save($data);
            // echo $this->getLastSql();exit;
            return $result;
        }
    }



    /**
     * 删除产品
     * @param  array $map where语句的数组形式
     * @return array      操作状态
     */
    public function deleteData($map){
        $this->where($map)->delete();
        $result=array(
            'error_code'=>0,
            'error_message'=>'操作成功'
            );
        return $result;
    }

    public function getCountData($uid,$goods_ids){
        
    }

// 获取用户分页数据
    public function getPageData($map){
        $count=$this->where($map)->count();
        $page=new \Org\Bjy\Page($count,20);
        $list=$this
            ->where($map)
            ->order('create_time desc')
            ->limit($page->firstRow.','.$page->listRows)
            ->select();
        $status = C('CHECK_STATUS');
        $product_type = M('cms_category')->where(array('category_type'=>1,'del_flag'=>'0'))->select();
        $order= C('ORDER_SATUTS');         
        foreach($list as $k => $v) {
            $product = D('ProductBasicInfo')->where(array('product_id'=>$v['product_id']))->field('product_type')->find();
            foreach ($product_type as $key => $value) {
                if($product['product_type']==$value['category_id']){ 
                    $list[$k]['product_type']=$value['category_name'];
                }
            } 
            $list[$k]['order_status'] = $order[$v['order_status']];
             
        }
        $data=array(
            'data'=>$list,
            'page'=>$page->show()
            );
        return $data;

    }

    public function getData($map){
        $data=$this->where($map)->find();
        return $data;
    }
}