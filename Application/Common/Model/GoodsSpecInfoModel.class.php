<?php
namespace Common\Model;
use Think\Model;
use Think\Upload;

/**
 * 文件模型
 * 负责文件的下载和上传
 */

class GoodsSpecInfoModel extends Model{
     // 自动验证
    protected $_validate=array(
        array('name','require','分类名称必填'), // 验证字段必填
        array('pid','require','上一级必填'), // 验证字段必填  
    );   
    protected $_auto=array(
        array('create_time', 'date',1,'function',array('Y-m-d H:i:s')),
        array('update_time', 'date',1,'function',array('Y-m-d H:i:s'))
        );
     /**
     * 添加用户
     */
    public function addData($data){
        // 对data数据进行验证
        // print_r($data);exit;
        if(!$data=$this->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            // 验证通过
            $result=$this->add($data);
            return $result;
        }
    }

    /**
     * 修改用户
     */
    public function editData($map,$data){
        // 对data数据进行验证
        if(!$data=$this->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            // 验证通过
            $result=$this
                ->where($map)
                ->save($data);
            return $result;
        }
    }


    // 获取佣金分页数据
    public function getPageData(){
        $count = M('ImageSpecInfo')->count();
        $page=new \Org\Bjy\Page($count,20);
        $list= M('ImageSpecInfo')
            ->order('create_time desc')
            ->limit($page->firstRow.','.$page->listRows)
            ->select();
        $data=array(
            'data'=>$list,
            'page'=>$page->show()
            );
        return $data;

    }


    /**
     * 删除数据
     * @param   array   $map    where语句数组形式
     * @return  boolean         操作是否成功
     */
    public function deleteData($map){
        $count=$this
            ->where(array('pid'=>$map['id']))
            ->count();
        if($count!=0){
            return false;
        }
        $this->where(array($map))->delete();
        return true;
    }

    public function getCateName($pid){
        $map = array('id'=>$pid);
        $data = $this->where($map)->field('id,name')->order('create_time asc')->find();
        return $data;
    }

    //获取全部分类列表
    public function getAllCateName(){
        $map = array('pid'=>0);
        $data = $this->where($map)->field('id,name')->order('create_time asc')->select();
        foreach ($data as $key => $val) {
            $data[$key]['cateName'] = $this->where(array('pid'=>$val['id']))->field('id,pid,name')->order('create_time asc')->select();
        }
        // print_r($data);exit;
        return $data;
    }    

    
}
