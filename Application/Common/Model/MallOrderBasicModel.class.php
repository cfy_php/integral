<?php
namespace Common\Model;
use Think\Model;
use Think\Upload;

/**
 * 文件模型
 * 负责文件的下载和上传
 */

class MallOrderBasicModel extends Model{
    protected $connection = 'DB_CONFIG2';
    // protected $dbName='dbcfy';
    // protected $tablePrefix = 't_';
    


     // protected $trueTableName='dbcfy.t_mall_order_basic';
     /**
     * 添加用户
     */
    public function addData($data){
        // 对data数据进行验证
        // print_r($data);exit;
        if(!$data=$this->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            // 验证通过
            $result=$this->add($data);
            return $result;
        }
    }

    /**
     * 修改用户
     */
    public function editData($map,$data){
        // 对data数据进行验证
        if(!$data=$this->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            // 验证通过
            $result=$this
                ->where($map)
                ->save($data);
            return $result;
        }
    }


    // 获取佣金分页数据
    public function getPageData(){
        // $count = $this->count();
        $count = $this->where(array('order_status'=>1))->count();
        $page=new \Org\Bjy\Page($count,20);
        $list= $this->where(array('order_status'=>1))
              ->order('create_time desc')
              ->limit($page->firstRow.','.$page->listRows)
              ->select();
        foreach ($list as $key => $value) {
            $list[$key]['username'] = M('user_goods_address','','DB_CONFIG2')->where(array('user_id'=>$value['user_id']))->getField('user_name');
        }
        $data=array(
            'data'=>$list,
            'page'=>$page->show()
            );
        return $data;

    }


    /**
     * 删除数据
     * @param   array   $map    where语句数组形式
     * @return  boolean         操作是否成功
     */
    public function deleteData($map){
        $count=$this
            ->where(array('pid'=>$map['id']))
            ->count();
        if($count!=0){
            return false;
        }
        $this->where(array($map))->delete();
        return true;
    }

    public function getAddress($map){
       $data =  M('user_goods_address','','DB_CONFIG2')->where($map)->find();
       $data['province_name'] = M('province','','DB_CONFIG2')->where(array('province_code'=>$data['province_code']))->getField('province_name');
       $data['city_name'] = M('city','','DB_CONFIG2')->where(array('city_code'=>$data['city_code']))->getField('city_name');
       return $data;
    }

    public function getGoods($map){
       $data =  M('goods_basic_info','','DB_CONFIG2')->where($map)->field('goods_brand,goods_name,goods_type')->find();
       if(!empty($data)){
         $list = array(1=>'实物类',2=>'虚拟类',3=>'视频');
         $data['goods_type'] = $list[$data['goods_type']];
       }
       return $data;
    }

}
