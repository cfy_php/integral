<?php

namespace Admin\Controller;
use Org\Util\Ftp;
/**
 * 文件控制器
 * 主要用于下载模型的文件上传和下载
 */
class FileController extends CommonController {

    /* 文件上传 */
    public function upload(){
        if(!empty($_FILES)){
            $data = $_FILES;
            foreach($data as $key => $val){
                if($key != "download"){
                    $data['download'] = $val;
                    unset($data[$key]);
                }
            }
        }
		$return  = array('status' => 1, 'info' => '上传成功', 'data' => '');
		/* 调用文件上传组件上传文件 */
		$File = D('FileInfo');
		$file_driver = C('DOWNLOAD_UPLOAD_DRIVER');
		$info = $File->upload(
			$data,
			C('DOWNLOAD_UPLOAD'),
			C('DOWNLOAD_UPLOAD_DRIVER'),
			C("UPLOAD_{$file_driver}_CONFIG")
		);
        // print_r($File->getError());exit;
        /* 记录附件信息 */
        if($info){
            $return = array_merge($info['download'], $return);
        } else {
            $return['status'] = 0;
            $return['info']   = $File->getError();
        }
        /* 返回JSON数据 */
        $this->ajaxReturn($return);
    }

    /* 下载文件 */
    public function download(){
        $id = I('get.id');
        if(empty($id) || !is_numeric($id)){
            $this->error('参数错误！');
        }
        $File = D('FileInfo');
        $root = C('DOWNLOAD_UPLOAD.rootPath');
        if(false === $File->download($root, $id)){
            $this->error = $File->getError();
        }

    }

    public function close_file(){
        $id = I("post.id");
        $data_id = I("post.data_id");
        if(empty($data_id)){
            $data=array(
              'status'=>'0',
            ); 
        }
        $result = D('FileInfo')->removefile($data_id,$id);
       if($result){
            $data=array(
              'status'=>'1',
            );
        }else{
            $data=array(
              'status'=>'0',
            );
        }
         $this->ajaxReturn($data);
    }
    public function removefile(){
        $id = I("post.id");
        $result = D('FileInfo')->removeThisfile($id);
       if($result){
            $data=array(
              'status'=>'1',
            );
        }else{
            $data=array(
              'status'=>'0',
            );
        }
         $this->ajaxReturn($data);
    }

   /**
     * 上传图片
     * @author huajie <banhuajie@163.com>
     */
    public function uploadPicture(){
        //TODO: 用户登录检测
        if(!empty($_FILES)){
            $data = $_FILES;
            foreach($data as $key => $val){
                if($key != "download"){
                    $data['download'] = $val;
                    unset($data[$key]);
                }
            }
        }        
        /* 返回标准数据 */
        $return  = array('status' => 1, 'info' => '上传成功', 'data' => '');

        /* 调用文件上传组件上传文件 */
        $Picture = D('FileUserInfo');
        $pic_driver = C('PICTURE_FTP_DRIVER');
        $info = $Picture->uploadPicture(
            $data,
            C('UPLOAD_FTP_PICTURE'),
            C('PICTURE_FTP_DRIVER'),
            C("UPLOAD_{$pic_driver}_CONFIG")
        ); //TODO:上传到远程服务器
        /* 记录图片信息 */
        if($info){
            $return['status'] = 1;
            $return = array_merge($info['download'], $return);
        } else {
            $return['status'] = 0;
            $return['info']   = $Picture->getError();
        }
         
        /* 返回JSON数据 */
        $this->ajaxReturn($return);

    }

   /**
     * 上传图片
     * @author huajie <banhuajie@163.com>
     */
    public function uploadBanner(){
        //TODO: 用户登录检测
        if(!empty($_FILES)){
            $data = $_FILES;
            foreach($data as $key => $val){
                if($key != "download"){
                    $data['download'] = $val;
                    unset($data[$key]);
                }
            }
        }     
        /* 返回标准数据 */
        $return  = array('status' => 1, 'info' => '上传成功', 'data' => '');

        /* 调用文件上传组件上传文件 */
        $Picture = D('GoodsBasicInfo');
        $pic_driver = C('PICTURE_UPLOAD_DRIVER');
        $info = $Picture->uploadPicture(
            $data,
            C('PICTURE_UPLOAD'),
            C('PICTURE_UPLOAD_DRIVER'),
            C("UPLOAD_{$pic_driver}_CONFIG")
        ); //TODO:上传到远程服务器
        /* 记录图片信息 */
        if($info){
            $return['status'] = 1;
            $return = array_merge($info['download'], $return);
        } else {
            $return['status'] = 0;
            $return['info']   = $Picture->getError();
        }

        /* 返回JSON数据 */
        $this->ajaxReturn($return);

    }

    public function open_pdf(){
        $id = I('id');
        $file = D('FileInfo')->where(array('id'=>$id))->find();
        $filespath = './Uploads/Download/'.$file['savepath'].$file['savename'];
        $fp = fopen($filespath, "r");
        header("Content-type: application/pdf");
        fpassthru($fp);
        fclose($fp);

    }


}
