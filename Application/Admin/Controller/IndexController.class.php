<?php
namespace Admin\Controller;
/**
 * 后台首页界面
 * 
 */
class IndexController extends CommonController {
   // 后台首页
    public function index(){
        // 分配菜单数据
        $nav_data=D('AdminNav')->getTreeData('level','order_number,id');
        $nav = array();
        foreach ($nav_data as $key => $value) { 
             $nav[$value['type']][$key] = $value;        
        }
        ksort($nav);
        // print_r($nav);exit;
        $list = D('AdminNav')->getNav($nav_data);
        $assign=array(
            'data'=>$nav,
            'nav' =>$list
            );
        $this->assign($assign);
        $this->display('index');
    }
    // 欢迎页面
    public function welcome(){
        $this->display('welcome');
    }

}