<?php
namespace Admin\Controller;
/**
 * 后台banner图片管理
 */
class OrderController extends CommonController{
	/**
	 * 菜单列表
	 */
	public function index(){
		$data=D('MallOrderBasic')->getPageData();
		// print_r($data);exit;
		$assign=array(
			'data'=>$data['data'],
			'page'=>$data['page']
			);
		$this->assign($assign);
		$this->display('index');
	}

   //添加物流快递信息
   public function addContract(){
     $data = I('post.');
     if($data['express_company'] == '' || $data['express_number'] == ''){
        $this->error('物流公司或物流单号必填');
     }
     $map = array(
        'order_id' => $data['order_id'],
      );
     $data['express_category'] = 2;
     if(!empty($data)){
        $result=D('OrderExpressInfo')->addData($data);
        if($result) {
        	$mall['express_status'] = 2;
            $res = D('MallOrderBasic')->editData($map,$mall);
            if($res){
                 $this->success('添加成功',U('Admin/Order/index'));    	
            }else{
	          $this->error('添加失败');
	        }
        }else{
          $this->error('添加失败');
        }
     }

   }

	/**
	 * 修改banner图片
	 */
	public function order_view(){
		$id = I('get.id');
		$info = D('MallOrderBasic')
		     ->where(array('order_id'=>$id))
		     ->find();
		$goods = D('MallOrderBasic')->getGoods(array('order_id'=>$id));
		$info['goods_brand'] = $goods['goods_brand'];
		$info['goods_type'] = $goods['goods_type'];
		if($info['express_status'] == 2){
			$map = array('order_id'=>$id,'express_category'=>2);
			$express = D('OrderExpressInfo')->getData($map);
			$this->assign('express',$express);
		}
		if(!empty($info['address_id'])){
			$address['address_id'] = $info['address_id'];
            $address = D('MallOrderBasic')->getAddress($address);
			$this->assign('address',$address);			
		}
		$this->assign('info',$info);
		$this->display('order_view');

	}

	/**
	 * 删除banner图片
	 */
	public function delete(){
		$id=I('get.id');
		$map=array(
			'id'=>$id
			);
		$result=M('MallOrderBasic')->where($map)->delete();
		if($result){
			$this->success('删除成功',U('Admin/Image/index'));
		}else{
			$this->error('删除失败');
		}
	}

}
