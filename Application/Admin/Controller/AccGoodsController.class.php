<?php
namespace Admin\Controller;
/**
 * 后台banner图片管理
 */
class AccGoodsController extends CommonController{
	/**
	 * 菜单列表
	 */
	public function index(){
    $apply = I('apply');
    if($apply == 1){
       $map['goods_status'] = array('in','1,3');
    }else{
       $map['goods_status'] = I('apply');
    }
    if(isset($_GET['nickname'])){
        $map['goods_name|goods_brand']=   array(array('like','%'.$_GET['nickname'].'%'),array('like','%'.$_GET['nickname'].'%'),array('like','%'.$_GET['nickname'].'%'),'_multi'=>true);
    }    
		$data=D('GoodsBasicInfo')->getPageData($map);
		$assign=array(
			'data'=>$data['data'],
			'page'=>$data['page'],
      'apply'=>$apply,
			);
		$this->assign($assign);
		$this->display('index');
	}

	/**
	 * 添加banner图片
	 */
	public function addSearchGoods(){
     if(!empty($_GET)){
         $map = array();
          if(isset($_GET['nickname'])){
              $map['b.goods_name|b.goods_brand']=   array(array('like','%'.$_GET['nickname'].'%'),array('like','%'.$_GET['nickname'].'%'),array('like','%'.$_GET['nickname'].'%'),'_multi'=>true);
              $res['b.goods_name|b.goods_brand|s.goods_number'] = array(array('like','%'.$_GET['nickname'].'%'),array('like','%'.$_GET['nickname'].'%'),array('like','%'.$_GET['nickname'].'%'),'_multi'=>true);
          }
          $res['s.status'] = 0;
          $res['b.goods_status'] = 0;
          $map['b.goods_status'] = 0;
          $data=D('GoodsBasicInfo')->getPageList($map,$res);
          if(!empty($data)){
              $data=array(
                    'status'=>1,
                    'data' =>$data['data'],
                    'page' =>$data['page']
              );
          }
          $this->ajaxReturn($data);
      }else{
        	$cate = D('GoodsSpecInfo')->getAllCateName();
        	$this->assign('cate',$cate);
        	$this->display('search');
      }

	}


    //商品添加
    public function spec_set(){
        $edit = I('edit');
        $data = I('post.');
        if($data['discount_point']>$data['goods_point']){
          $this->error('优惠的积分数不能大于兑换商品需要的积分数');
        }
        if($edit == 0){
          if($data['limit_num']>$data['goods_total']){
            $this->error('商品数量不能大于'.$data['goods_total']);
          }          
          $data['goods_total'] = $data['goods_total'] - $data['limit_num'];
          $data['goods_status'] = 1;          
        }else{
          if($data['add_num']>$data['goods_total']){
            $this->error('商品追加数量不能大于'.$data['goods_total']);
          }               
          $data['goods_total'] = $data['goods_total'] - $data['add_num'];
          $data['limit_num'] =  $data['limit_num']+$data['add_num'];
        }
        $map['goods_id'] = $data['goods_id'];
        $res = D('GoodsBasicInfo')->editData($map,$data);
        if($res){
          $this->success('保存成功',U('Admin/AccGoods/index?apply=1'));
        }else{
          $this->error('保存失败');
        }
   
    }
  

  public function edit() {
      $map['goods_id'] = I('id');
      $edit = I('edit');
      $info = D('GoodsBasicInfo')->where($map)->find();
      $this->assign('info',$info);
      $this->assign('edit',$edit);
      if($edit==3){
         $this->display('goods_view');
      }else{
        $this->display('add');    
      }
  }


  public function checkGoods(){
     $status = I('status');
     $map['goods_id'] = I('id');
     if($status == 0){
        $info = D('GoodsBasicInfo')->where($map)->field('goods_total,limit_num')->find();
        $data['goods_total'] = $info['goods_total'] + $info['limit_num'];
        $data['limit_num'] = '';
     }
     $data['goods_status'] = $status;
      $res = D('GoodsBasicInfo')->editData($map,$data);
      if($res){
        $this->success('提交成功');
      }else{
        $this->error('提交失败');
      }     
  }




 //  public function addGoods() {
 //     if(!empty($_GET)){
 //        $map['goods_id'] = I('id');
 //        $info = D('GoodsBasicInfo')->where($map)->find();
 //        if($info['goods_spec'] == 1){
 //          $setData = M('goods_spec_set_info')->where($map)->select();
 //          foreach ($setData as $key => $value) {
 //            $mapping[$key]['mapping_id']= M('goods_spec_set_detail as d')
 //                  ->join('t_goods_spec_info as i on i.id = d.mapping_id')
 //                  ->where(array('set_id'=>$value['set_id']))
 //                  ->field('mapping_id,name')
 //                  ->select();
 //          }    
 //          $this->assign('edit',1);
 //          $this->assign('mapping',$mapping);
 //          $this->assign('list',$setData);
 //          $cate = D('GoodsSpecInfo')->getAllCateName();
 //          $this->assign('cate',$cate);          
 //        }
 //        $this->assign('info',$info);
 //        $this->display('addGoods');
 //     }
 //  }

	// /**
	//  * 修改banner图片
	//  */
	// public function add(){

	// 	$id = I('get.id');
	// 	$info = M('GoodsBasicInfo as b')
	// 	     ->join('t_file_info as f on b.goods_pic = f.id','left')
	// 	     ->field('b.*,f.savepath,f.savename')
	// 	     ->where(array('b.goods_id'=>$id))
	// 	     ->find();
	// 	$info['goods_desc'] = preg_ueditor_image_path($info['goods_desc']);
	// 	if($info['goods_type'] == 1 && $info['goods_spec'] == 1 ){
	//         $setData = M('goods_spec_set_info as s')->where(array('s.goods_id'=>$id))->select();
	//         foreach ($setData as $key => $value) {
	// 	        $mapping[$key]['mapping_id']= M('goods_spec_set_detail')
	// 	              ->where(array('set_id'=>$value['set_id']))
	// 	              ->field('mapping_id')
	// 	              ->select();
	//         }
 //            $this->assign('mapping',$mapping);
 //        	$this->assign('list',$setData);
 //    		$this->assign('edit',1);			
	// 	}
 //    $cate = D('GoodsSpecInfo')->getAllCateName();
	// 	$this->assign('info',$info);
	// 	$this->assign('cate',$cate);
	// 	$this->display('editGoods');

	// }

	// /**
	//  * 删除banner图片
	//  */
	// public function delete(){
	// 	$id=I('get.id');
	// 	$map=array(
	// 		'id'=>$id
	// 		);
	// 	$result=M('GoodsBasicInfo')->where($map)->delete();
	// 	if($result){
	// 		$this->success('删除成功',U('Admin/Goods/index'));
	// 	}else{
	// 		$this->error('删除失败');
	// 	}
	// }



 //    //商品规则修改
 //    public function SpecEditset(){
 //      $data = I('post.');
 //    	foreach ($data as $key => $value) {
 //    		$code = explode('_', $key);
 //    		if(array_key_exists(2,$code)){
 //              $arr[] = $code[2];
 //    		}else{
 //          if(!is_array($value)){
 //              $goods[$key] = $value;
 //          }
 //        }
 //    	}
 //      $map['goods_id'] = $data['goods_id'];
 //      $goods = filter_array($goods);
 //      $goods['goods_status'] = 1;
 //      $res = D('GoodsBasicInfo')->editData($map,$goods);
 //    	$arr =array_unique($arr); 
 //    	$list = D('GoodsSpecSetInfo')->formData($data,$arr);    
 //    	foreach ($list as $key => $value) {
 //    	     $map['goods_id'] = $data['goods_id']; 
 //    	     $map['set_id'] = $value['set_id'];
 //           $aid = D('GoodsSpecSetInfo')->SpecData($map,$value);
 //    	}
 //     	if($aid){
 //  			$this->success('添加成功',U('Admin/Goods/index'));
 //  		}else{
 //  			$this->error('添加失败');
 //  		}   	
 //    }
    

}
