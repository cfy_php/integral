<?php
namespace Admin\Controller;
/**
 * 后台banner图片管理
 */
class GoodsController extends CommonController{
	/**
	 * 菜单列表
	 */
	public function index(){
    $apply = I('apply');
    if($apply == 1){
       $map['goods_status'] = array('in','1,3');
    }else{
       $map['goods_status'] = I('apply');
    }
		$data=D('GoodsBasicInfo')->getPageData($map);
		$assign=array(
			'data'=>$data['data'],
			'page'=>$data['page']
			);
		$this->assign($assign);
		$this->display('index');
	}

	/**
	 * 添加banner图片
	 */
	public function addSearchGoods(){
     if(!empty($_GET)){
         $map = array();
          if(isset($_GET['nickname'])){
              $map['b.goods_name|b.goods_brand']=   array(array('like','%'.$_GET['nickname'].'%'),array('like','%'.$_GET['nickname'].'%'),array('like','%'.$_GET['nickname'].'%'),'_multi'=>true);
              $res['b.goods_name|b.goods_brand|s.goods_number'] = array(array('like','%'.$_GET['nickname'].'%'),array('like','%'.$_GET['nickname'].'%'),array('like','%'.$_GET['nickname'].'%'),'_multi'=>true);
          }
          $res['s.status'] = 0;
          $res['b.goods_status'] = 0;
          $map['b.goods_status'] = 0;
          $data=D('GoodsBasicInfo')->getPageList($map,$res);
          if(!empty($data)){
              $data=array(
                    'status'=>1,
                    'data' =>$data['data'],
                    'page' =>$data['page']
              );
          }
          $this->ajaxReturn($data);
      }else{
        	$cate = D('GoodsSpecInfo')->getAllCateName();
        	$this->assign('cate',$cate);
        	$this->display('search');
      }

	}





  public function add() {
     if(!empty($_GET)){
        $map['goods_id'] = I('id');
        $info = D('GoodsBasicInfo')->where($map)->find();
        $this->assign('info',$info);
        $this->display('add');
     }
  }


  public function addGoods() {
     if(!empty($_GET)){
        $map['goods_id'] = I('id');
        $info = D('GoodsBasicInfo')->where($map)->find();
        if($info['goods_spec'] == 1){
          $setData = M('goods_spec_set_info')->where($map)->select();
          foreach ($setData as $key => $value) {
            $mapping[$key]['mapping_id']= M('goods_spec_set_detail as d')
                  ->join('t_goods_spec_info as i on i.id = d.mapping_id')
                  ->where(array('set_id'=>$value['set_id']))
                  ->field('mapping_id,name')
                  ->select();
          }    
          $this->assign('edit',1);
          $this->assign('mapping',$mapping);
          $this->assign('list',$setData);
          $cate = D('GoodsSpecInfo')->getAllCateName();
          $this->assign('cate',$cate);          
        }
        $this->assign('info',$info);
        $this->display('addGoods');
     }
  }

	/**
	 * 修改banner图片
	 */
	public function edit(){
      $map['goods_id'] = I('id');
      $info = D('GoodsBasicInfo')->where($map)->find();
      $this->assign('info',$info);
      $this->display('add');
	}

	/**
	 * 删除banner图片
	 */
	public function delete(){
		$id=I('get.id');
		$map=array(
			'id'=>$id
			);
		$result=M('GoodsBasicInfo')->where($map)->delete();
		if($result){
			$this->success('删除成功',U('Admin/AccGoods/index'));
		}else{
			$this->error('删除失败');
		}
	}


    //商品规则添加
    public function spec_set(){
        $data = I('post.');
        $data['goods_total'] = $data['goods_total'] - $data['limit_point'];
        $data['goods_status'] = 1;
        $map['goods_id'] = $data['goods_id'];
      	$res = D('GoodsBasicInfo')->editData($map,$goods);
      	if($res){
    			$this->success('保存成功',U('Admin/AccGoods/index'));
    		}else{
    			$this->error('保存失败');
    		}
    }
  

    //商品规则修改
    public function SpecEditset(){
      $data = I('post.');
    	foreach ($data as $key => $value) {
    		$code = explode('_', $key);
    		if(array_key_exists(2,$code)){
              $arr[] = $code[2];
    		}else{
          if(!is_array($value)){
              $goods[$key] = $value;
          }
        }
    	}
      $map['goods_id'] = $data['goods_id'];
      $goods = filter_array($goods);
      $goods['goods_status'] = 1;
      $res = D('GoodsBasicInfo')->editData($map,$goods);
    	$arr =array_unique($arr); 
    	$list = D('GoodsSpecSetInfo')->formData($data,$arr);    
    	foreach ($list as $key => $value) {
    	     $map['goods_id'] = $data['goods_id']; 
    	     $map['set_id'] = $value['set_id'];
           $aid = D('GoodsSpecSetInfo')->SpecData($map,$value);
    	}
     	if($aid){
  			$this->success('添加成功',U('Admin/AccGoods/index'));
  		}else{
  			$this->error('添加失败');
  		}   	
    }
    

}
