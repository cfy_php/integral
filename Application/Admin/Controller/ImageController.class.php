<?php
namespace Admin\Controller;
/**
 * 后台banner图片管理
 */
class ImageController extends CommonController{
	/**
	 * 菜单列表
	 */
	public function index(){
		$data=D('ImageSpecInfo')->getPageData();
		$assign=array(
			'data'=>$data['data'],
			'page'=>$data['page']
			);
		$this->assign($assign);
		$this->display('index');
	}

	/**
	 * 添加banner图片
	 */
	public function add(){
		$data=I('post.');
        if(!empty($data)){       	
        	if(empty($data['id'])){
		        $result=D('ImageSpecInfo')->addData($data);
				if ($result) {
					$this->success('添加成功',U('Admin/Image/index'));
				}else{
					$this->error('添加失败');
				}
        	}else{
		    	$map = array('id'=>$data['id']);
		        $result=D('ImageSpecInfo')->editData($map,$data);
				if ($result) {
					$this->success('修改成功',U('Admin/Image/index'));
				}else{
					$this->error('修改失败');
				}
        	}

        }else{
        	$this->display('add');
        }

	}

	/**
	 * 修改banner图片
	 */
	public function edit(){

		$id = I('get.id');
		$info = M('ImageSpecInfo')
		     ->where(array('id'=>$id))
		     ->find();
		$this->assign('info',$info);
		$this->display('add');

	}

	/**
	 * 删除banner图片
	 */
	public function delete(){
		$id=I('get.id');
		$map=array(
			'id'=>$id
			);
		$result=M('BannerDataList')->where($map)->delete();
		if($result){
			$this->success('删除成功',U('Admin/Image/index'));
		}else{
			$this->error('删除失败');
		}
	}

}
